var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

// req is the request object
// res is the response object
// next passes requests through the middleware layers until it matches something
// or ends up with a 404 not found error
router.get('/tutorial', function (req, res, next) {
	res.render('tutorial')
})

router.post('/create-user', function (req, res, next) {
	// access the post body through req.body. you can set it to
	// a variable, pass fields of the object into other functions
	// parse specific values from it, you know, anything you can
	// do with an object
	console.log(req.body)
	console.log(req.body.fn)

	// send a response to the browser. you must send some kind of repsonse
	// whether it is with res.end, res.send, res.sendFile, res.render or res.json
	// These different responses are shortcuts for sending different kinds of information
	res.json({message: 'recieved new user data on the server.'})
})

module.exports = router;
