// Declare an Angular app
var app = angular.module('app', [])

// Declare an Angular controller
app.controller('appCtrl', ['$scope', '$http', function($scope, $http) {
	$scope.number = 4
	console.log('my controller has loaded')
	console.log('$scope.number is ', $scope.number)

	// HTML submits data to this function
	$scope.myFuncName = function (fn, ln, email) {
		// print this to the js console.
		console.log(fn, ln, email)

		// make object out of user data
		var userData = {
			fn: fn,
			ln: ln,
			email: email
		}

		// make a request to the server at the specified url
		$http.post('http://localhost:3000/create-user', userData).
			then(function (response) {
				// the response object contains a lot of info. we just want the data rn.
				// see https://docs.angularjs.org/api/ng/service/$http for more about it.
				console.log(response.data)
			}, function (response) {
				console.log(response)
			})
	}
}])